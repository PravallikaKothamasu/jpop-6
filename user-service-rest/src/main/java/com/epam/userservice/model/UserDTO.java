package com.epam.userservice.model;

import io.swagger.annotations.ApiModelProperty;

public class UserDTO {
	@ApiModelProperty(notes = "userName of the userDTO", required = true, name = "userName", value = "test userName")
	private String userName;
	@ApiModelProperty(notes = "password of the userDTO", required = true, name = "password", value = "test password")
	private String password;

	public UserDTO(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDTO [userName=" + userName + ", password=" + password + "]";
	}
	
	
	

}
