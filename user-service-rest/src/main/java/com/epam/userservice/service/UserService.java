package com.epam.userservice.service;

import com.epam.userservice.exceptions.NoUserFoundException;
import com.epam.userservice.exceptions.NoUsersException;
import com.epam.userservice.model.User;
import com.epam.userservice.model.UserDTO;
import com.epam.userservice.repository.UserRepository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    
    private static final Logger LOG = LogManager.getLogger(UserService.class);

    public List<User> getAllUsers() {
    	LOG.info("inside get all users");
        List<User> users = userRepository.findAll();
        if (users.isEmpty()) {
        	LOG.info("users are not retreived as no users are present");
            throw new NoUsersException("No users are present");
        } else {
        	LOG.info("users:{} are retrieved",users);
            return users;
        }
    }

    public User getUserById(int userId) {
    	LOG.info("inside get user by id");
        return userRepository.findById(userId)
                .orElseThrow(() -> new NoUserFoundException("No user with " + userId + " is present"));
    }

    public User addUser(UserDTO userDTO) {
    	LOG.info("inside add user and user to be added is: {}",userDTO);
        return userRepository.save(map(userDTO));
    }

    public void deleteUser(int userId) {
    	LOG.info("inside delee user and userId to be deleted is: {}",userId);
        userRepository.delete(getUserById(userId));
    }

    public User updateUser(int userId, UserDTO userDTO) {
        if (userRepository.existsById(userId)) {
        	LOG.info("inside update user and user to update is: {}",userId);
            User userToBeUpdated = map(userDTO);
            userToBeUpdated.setUserId(userId);
            LOG.info(userToBeUpdated);
            return userRepository.save(userToBeUpdated);
        } else {
        	LOG.info("cannot update user with userID: {} as user is not present",userId);
            throw new NoUserFoundException("No user with " + userId + " is found");
        }
    }

    public User map(UserDTO userDTO) {
        User user = new User();
        user.setUserName(userDTO.getUserName());
        user.setPassword(userDTO.getPassword());
        return user;
    }
}
