package com.epam.userservice.service;

import com.epam.userservice.exceptions.NoUserFoundException;
import com.epam.userservice.exceptions.NoUsersException;
import com.epam.userservice.model.User;
import com.epam.userservice.model.UserDTO;
import com.epam.userservice.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = UserService.class)
class UserServiceTest {

    @Autowired
    UserService userService;
    @MockBean
    UserRepository userRepository;

    List<User> users;
    List<User> emptyUsers;
    User[] optionalUserArray;
    UserDTO userDTO;
    User userToBeAdded;
    User userAdded;

    @BeforeEach
    public void setup() {
        users = new ArrayList<>();
        emptyUsers = new ArrayList<>();
        users.add(new User(1, "Kvlss Pravallika", "pravallika@123"));
        users.add(new User(2, "pravallika kothamasu", "epam@123"));
        optionalUserArray = new User[10];
        optionalUserArray[0] = new User(1, "Kvlss Pravallika", "pravallika@123");
        userDTO = new UserDTO("abcdef", "12345");
        userToBeAdded = new User(1, "abcdef", "12345");
        userAdded = new User(1, "abcdef", "12345");
    }

    @Test
    public void testGetAllUsers() {
        when(userRepository.findAll()).thenReturn(users);
        assertEquals(users.size(), userService.getAllUsers().size());
    }

    @Test
    public void testGetAllUsersWhenEmpty() {
        when(userRepository.findAll()).thenReturn(emptyUsers);
        assertThrows(NoUsersException.class, () -> userService.getAllUsers());
    }

    @Test
    public void testGetUserById() {
        Optional<User> optionalUser = Optional.ofNullable(optionalUserArray[0]);
        when(userRepository.findById(1)).thenReturn(optionalUser);
        assertEquals(1, userService.getUserById(1).getUserId());
    }

    @Test
    public void testGetUserByIdWhenInvalidUserIdIsGiven() {
        Optional<User> optionalUser = Optional.ofNullable(optionalUserArray[1]);
        when(userRepository.findById(3)).thenReturn(optionalUser);
        assertThrows(NoUserFoundException.class, () -> userService.getUserById(3));
    }

    @Test
    public void testAddUser() {
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(userToBeAdded);
        assertEquals(userToBeAdded.getUserName(), userService.addUser(userDTO).getUserName());
    }

    @Test
    public void testDeleteUser() {
        Optional<User> optionalUser = Optional.ofNullable(optionalUserArray[0]);
        when(userRepository.findById(1)).thenReturn(optionalUser);
        doNothing().when(userRepository).delete(userToBeAdded);
        userService.deleteUser(1);
    }

    @Test
    public void testDeleteUserWhenUserNotPresent() {
        Optional<User> optionalUser = Optional.ofNullable(optionalUserArray[1]);
        when(userRepository.findById(3)).thenReturn(optionalUser);
        assertThrows(NoUserFoundException.class, () -> userService.deleteUser(3));
    }

    @Test
    public void testUpdateUser() {
        when(userRepository.existsById(1)).thenReturn(true);
        when(userRepository.save(ArgumentMatchers.any())).thenReturn(userToBeAdded);
        assertEquals(userToBeAdded.getUserName(), userService.updateUser(1, userDTO).getUserName());
    }

    @Test
    public void testUpdateUserWhenUserIsNotPresent() {
        when(userRepository.existsById(3)).thenReturn(false);
        assertThrows(NoUserFoundException.class, () -> userService.updateUser(1, userDTO));
    }

}
