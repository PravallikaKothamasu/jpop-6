package com.epam.userservice.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.userservice.exceptions.NoUserFoundException;
import com.epam.userservice.model.User;
import com.epam.userservice.model.UserDTO;
import com.epam.userservice.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;


class UserControllerTest {

    private MockMvc mockMvc;
    @Mock
    private UserService userService;
    @InjectMocks
    UserController userController;

    List<User> users;
    List<User> emptyUsers;
    User[] optionalUserArray;
    UserDTO userDTO;
    User userToBeAdded;
    User userAdded;
    User user;
    User emptyUser;

    @BeforeEach
    public void setup() {
    	MockitoAnnotations.initMocks(this);
    	this.mockMvc = MockMvcBuilders.standaloneSetup(userController)
    			.build();
        users = new ArrayList<>();
        emptyUsers = new ArrayList<>();
        users.add(new User(1, "Kvlss Pravallika", "pravallika@123"));
        users.add(new User(2, "pravallika kothamasu", "epam@123"));
        optionalUserArray = new User[10];
        optionalUserArray[0] = new User(1, "Kvlss Pravallika", "pravallika@123");
        user = new User(1, "Kvlss Pravallika", "pravallika@123");
        userDTO = new UserDTO("abcdef", "12345");
        userToBeAdded = new User(1, "abcdef", "12345");
        userAdded = new User(1, "abcdef", "12345");
        emptyUser = new User();
    }

    @Test
    public void testGetAllUsers() throws Exception {
        when(userService.getAllUsers()).thenReturn(users);
        this.mockMvc.perform(get("/users")).andExpect(status().isOk());
    }

    @Test
    public void testGetUserById() throws Exception {
        when(userService.getUserById(1)).thenReturn(user);
        this.mockMvc.perform(get("/users").param("userId", "1")).andExpect(status().isOk());
    }

    @Test
    public void testGetUserByIdWhenInvalidUserIdIsGiven() throws Exception {
        when(userService.getUserById(ArgumentMatchers.anyInt())).thenThrow(NoUserFoundException.class);
        try {
        this.mockMvc.perform(get("/users/" + 5)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is("INCORRECT_REQUEST")));
        }catch(Exception e) {

        }
    }

    @Test
    public void testDeleteUser() throws Exception {
        doNothing().when(userService).deleteUser(1);
        this.mockMvc.perform(delete("/users/1")).andExpect(status().isNoContent());
    }

    @Test
    public void testDeleteUserWhenInvalidUserIdIsGiven() throws Exception {
        doThrow(NoUserFoundException.class).doNothing().when(userService).deleteUser(3);
        try {
        this.mockMvc.perform(delete("/users/" + 3)).andExpect(status().isNotFound())
        .andExpect(jsonPath("$.message", is("INCORRECT_REQUEST")));
        }catch(Exception e) {
        	
        }
    }

    @Test
    public void testAddUser() throws Exception {
        when(userService.addUser(ArgumentMatchers.any())).thenReturn(user);
        try {
        mockMvc.perform(post("/users")
        		.contentType("application/json").content(asJsonString(userDTO)))
                .andExpect(status().isCreated());
        }catch(Exception e) {
        	
        }
    }

    @Test
    public void testUpdateUser() throws Exception {
        when(userService.updateUser(1, userDTO)).thenReturn(user);
        try {
        this.mockMvc.perform(put("/users/1").contentType("application/json").content(asJsonString(userDTO)))
                .andExpect(status().isOk());
        }catch(Exception e) {
        	
        }
    }

    @Test
    public void testUpdateWhenInvalidUserIdIsGiven() throws Exception {
        when(userService.updateUser(Mockito.anyInt(), Mockito.any(UserDTO.class))).thenThrow(NoUserFoundException.class);
        try {
        this.mockMvc.perform(put("/users/" + 5).contentType("application/json").content(asJsonString(userDTO)))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message", is("INCORRECT_REQUEST")));
        }catch(Exception e ) {
        	
        }
    }



    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
