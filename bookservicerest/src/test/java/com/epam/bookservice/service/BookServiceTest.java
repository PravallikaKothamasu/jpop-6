package com.epam.bookservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.bookservice.exceptions.NoBooksException;
import com.epam.bookservice.exceptions.NoSuchBookFound;
import com.epam.bookservice.model.Book;
import com.epam.bookservice.model.BookDTO;
import com.epam.bookservice.repository.BookRepository;

class BookServiceTest {

	@InjectMocks
	BookService bookService;
	@Mock
	BookRepository bookRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	List<Book> books;
	Book[] optionalBookArray;
	BookDTO bookDTO;
	public void setup()
	{
		books = new ArrayList<>();
		books.add(new Book(1, "The Hobbit", "J.R.R.Tolkein", "Thriller", (long) 1000));
		books.add(new Book(2, "Slinky Malinki", "Lynley Dodd", "ScienceFiction", (long) 500));
		books.add(new Book(3, "Revolution 2020", "Chetan Bhagat", "Novel", (long) 1000));
		books.add(new Book(4, "Half-Girlfriend", "Chetan Bhagat", "Fiction", (long) 800));
		books.add(new Book(5, "Spring microservices", "John Carnell", "Technology", (long) 500));
		books.add(new Book(6, "Spring in action", "Craig Walls", "Technology", (long) 1200));
		books.add(new Book(7, "Mechanical Harry", "Bob Herr", "Science", (long) 500));
		optionalBookArray = new Book[10];
		optionalBookArray[0] = new Book(1, "The Hobbit", "J.R.R.Tolkein", "Thriller", (long) 1000);
		bookDTO = new BookDTO(1, "Lost", "unknown", "comedy", (long)100);
	}

	@Test
	public void testFindAllBooks() {
		setup();
		when(bookRepository.findAll()).thenReturn(books);
		assertEquals(books.size(), bookService.findAllBooks().size());
	}
	
	@Test
	public void testFindAllBooksWhenThereAreNoBooks() {
		List<Book> books = new ArrayList<>();
		when(bookRepository.findAll()).thenReturn(books);
		assertThrows(NoBooksException.class, ()->bookService.findAllBooks());
	}
	
	@Test
	public void testGetBookById() {
		setup();
		Optional<Book> optionalBook = Optional.ofNullable(optionalBookArray[0]);
		when(bookRepository.findById(1)).thenReturn(optionalBook);
		assertEquals(1, bookService.getBookById(1).getBookId());
	}
	
	@Test
	public void testGetBookByIdWhenInvalidIdIsGiven()
	{
		setup();
		Optional<Book> optionalBook = Optional.ofNullable(optionalBookArray[1]);
		when(bookRepository.findById(1)).thenReturn(optionalBook);
		assertThrows(NoSuchBookFound.class, ()->bookService.getBookById(1));
	}
	
	@Test
	public void testAddBook() {		
		setup();
		Book book = new Book(1, "Lost", "unknown", "comedy", (long)100);
		when(bookRepository.save(book)).thenReturn(book);
		bookService.addBook(bookDTO);
	}
	
	@Test
	public void testDeleteBookById() {
		when(bookRepository.existsById(1)).thenReturn(true);
		doNothing().when(bookRepository).deleteById(1);
		bookService.deleteBookById(1);
	}
	
	@Test
	public void testDeleteBookByIdWhenBookIdIsNotPResent() {
		when(bookRepository.existsById(1)).thenReturn(false);
		assertThrows(NoSuchBookFound.class, ()->bookService.deleteBookById(1));
	}
	
	@Test
	public void testUpdateBookById() {
		setup();
		when(bookRepository.existsById(1)).thenReturn(true);
		Book book = new Book(1, "Lost", "unknown", "unknown", (long)100);
		when(bookRepository.save(book)).thenReturn(book);
		bookService.updateBookById(1, bookDTO);
	}
	
	@Test
	public void testUpdateBookByIdWhenIdIsNotPresent() {
		setup();
		when(bookRepository.existsById(1)).thenReturn(false);
		assertThrows(NoSuchBookFound.class, ()->bookService.updateBookById(1, bookDTO));
	}

}
