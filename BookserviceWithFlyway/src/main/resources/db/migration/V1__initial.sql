CREATE TABLE book (
  bookId int NOT NULL AUTO_INCREMENT,
  name varchar(100) NOT NULL,
  cost int NOT NULL,
  PRIMARY KEY (bookId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;