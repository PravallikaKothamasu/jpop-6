package com.epam.bookserviceWithFlyway;

import javax.persistence.*;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int bookId;

    @Column(name = "name")
    private String name;

    @Column(name = "cost")
    private int cost;

    @Column(name = "author_name")
    private String author_name;

    public int getBookId() {
        return bookId;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public Book(int bookId, String name, int cost) {
        this.bookId = bookId;
        this.name = name;
        this.cost = cost;
    }

    public Book() {

    }
}
