package com.epam.bookserviceWithFlyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookserviceWithFlywayApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookserviceWithFlywayApplication.class, args);
	}

}
