package com.epam.awss3.controller;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.epam.awss3.Person;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/s3")
public class AwsS3Controller {

    @Autowired
    private ObjectMapper objectMapper;

    private static final Logger logger = LogManager.getLogger(AwsS3Controller.class);

    private String bucketNotAvailableMessage = "bucket name is not available. please try with another bucket name";

    @Autowired
    private AmazonS3 awsS3Client;

    @PostMapping("/create-bucket/{bucketName}")
    public String createBucket(@PathVariable String bucketName) {
        String message;
        if(awsS3Client.doesBucketExistV2(bucketName)){
            logger.info(bucketNotAvailableMessage);
            message = bucketNotAvailableMessage;
        }else{
            awsS3Client.createBucket(bucketName);
            message = "bucket created successfully";
        }
        return message;
    }

    @PostMapping("/upload/bucket/{bucketName}")
    public String uploadToS3(@PathVariable String bucketName){
        //uploading file
//        awsS3Client.putObject(bucketName, "sample.png",
//                new File("C:/Users/Pravallika_Kothamasu/Pictures/Screenshots/Screenshot (59).png"));
        // uploading object
        awsS3Client.putObject(bucketName,"person.java", new Person("Kvlss", "Pravallika").toString());
    return "object uploded";
    }

    @GetMapping("/fetch/{bucketName}/object/{objectKey}")
    public String getObject(@PathVariable String bucketName,
                            @PathVariable String objectKey) throws IOException {
        S3Object s3Object = awsS3Client.getObject(bucketName, objectKey);
        String fetched = new String(s3Object.getObjectContent().readAllBytes());
        logger.info("fetched object: {}", fetched);
        Person person = objectMapper.readValue(fetched, Person.class);
        logger.info("object: {}", person);
        return "fetched object";
    }
}
