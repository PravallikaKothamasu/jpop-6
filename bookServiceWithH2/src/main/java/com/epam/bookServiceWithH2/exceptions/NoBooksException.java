package com.epam.bookServiceWithH2.exceptions;

public class NoBooksException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public NoBooksException(String message) {
		super(message);
	}

}
