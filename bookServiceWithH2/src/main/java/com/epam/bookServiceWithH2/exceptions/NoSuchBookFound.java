package com.epam.bookServiceWithH2.exceptions;

public class NoSuchBookFound extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchBookFound(String message) {
		super(message);
	}

}
