package com.epam.bookServiceWithH2.service;

import com.epam.bookServiceWithH2.exceptions.NoBooksException;
import com.epam.bookServiceWithH2.exceptions.NoSuchBookFound;
import com.epam.bookServiceWithH2.model.Book;
import com.epam.bookServiceWithH2.model.BookDTO;
import com.epam.bookServiceWithH2.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public List<Book> findAllBooks() {
		List<Book> booksList =  bookRepository.findAll();
		if (booksList.isEmpty()) {
			throw new NoBooksException("No books are present");
		} else {
			return booksList;
		}
	}

	public Book getBookById(int bookId) {
		return bookRepository.findById(bookId)
				.orElseThrow(() -> new NoSuchBookFound("No such book with id: " + bookId + " is found"));
	}

	public Book addBook(BookDTO bookToBeAdded) {
		Book book = map(bookToBeAdded);
		return bookRepository.save(book);
	}

	public void deleteBookById(int bookId) {
		if (bookRepository.existsById(bookId)) {
			bookRepository.deleteById(bookId);
		} else {
			throw new NoSuchBookFound("No book with " + bookId + " is found");
		}

	}

	public Book updateBookById(int bookId, BookDTO bookDTO) {
		if(bookRepository.existsById(bookId)) {
		Book bookToBeUpdated = map(bookDTO);
		bookToBeUpdated.setBookId(bookId);
		return bookRepository.save(bookToBeUpdated);
		}else {
			throw new NoSuchBookFound("No book with " + bookId + " is found");
		}
	}
	
	public Book map(BookDTO bookDTO) {
		Book book = new Book();
		book.setAuthor(bookDTO.getAuthor());
		book.setTitle(bookDTO.getTitle());
		book.setCost(bookDTO.getCost());
		book.setGenre(bookDTO.getGenre());
		return book;
	}
}
