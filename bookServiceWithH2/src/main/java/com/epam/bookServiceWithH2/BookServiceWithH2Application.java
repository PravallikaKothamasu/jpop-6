package com.epam.bookServiceWithH2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class BookServiceWithH2Application {

	public static void main(String[] args) {
		SpringApplication.run(BookServiceWithH2Application.class, args);
	}

}
