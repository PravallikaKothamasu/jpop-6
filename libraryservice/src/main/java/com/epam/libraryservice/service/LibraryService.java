package com.epam.libraryservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.libraryservice.exceptions.AlreadyIssuedException;
import com.epam.libraryservice.exceptions.NoSuchBookIssued;
import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.IssueBook;
import com.epam.libraryservice.repositories.IssueBookRepository;

@Service
public class LibraryService {

	@Autowired
	BookServiceClient bookServiceClient;

	@Autowired
	UserServiceClient userServiceClient;

	@Autowired
	IssueBookRepository issueBookRepository;

	public IssueBook issueBookToAUser(int userId, int bookId) {
		if (checkIfBookIsNotIssuedToUser(userId, bookId) && isUserPresent(userId) && isBookPresent(bookId)) {
			IssueBook bookToBeIssued = new IssueBook();
			bookToBeIssued.setBookId(bookId);
			bookToBeIssued.setUserId(userId);
			return issueBookRepository.save(bookToBeIssued);
		} else {
			throw new AlreadyIssuedException(
					"Book with bookId: " + bookId + " is already issued to user with userID: " + userId);
		}
	}

	public boolean isUserPresent(int userId) {
		int userIdToValidate = userServiceClient.getUserByIdByLibraryService(userId).getBody().getUserId();
		return userIdToValidate == userId;

	}

	public boolean isBookPresent(int bookId) {
		int bookIdToValidate = bookServiceClient.getBookByIdByLibraryService(bookId).getBody().getBookId();
		return bookIdToValidate == bookId;
	}

	public boolean checkIfBookIsNotIssuedToUser(int userId, int bookId) {
		List<IssueBook> booksOfUser = issueBookRepository.findByUserId(userId);
		List<IssueBook> booksAllreadyIssuedToUser = booksOfUser.stream().filter(book -> book.getBookId() == bookId)
				.collect(Collectors.toList());
		return booksAllreadyIssuedToUser.isEmpty();
	}

	public void releaseTheBookforUser(int userId, int bookId) {
		if (isUserPresent(userId) && isBookPresent(bookId) && !checkIfBookIsNotIssuedToUser(userId, bookId)) {
			List<IssueBook> booksOfUser = issueBookRepository.findByUserId(userId);
			List<IssueBook> bookToBeReleased = booksOfUser.stream().filter(book -> book.getBookId() == bookId)
					.collect(Collectors.toList());
			bookToBeReleased.forEach(book -> issueBookRepository.deleteById(book.getIssueId()));
		} else {
			throw new NoSuchBookIssued("No book with bookId: " + bookId + " is issed to user with userId: " + userId);
		}
	}
}
