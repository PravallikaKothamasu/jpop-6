package com.epam.libraryservice.interfaces;

import com.epam.libraryservice.fallbackfactory.UserServiceClientFallback;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@FeignClient(name = "user-service",fallback = UserServiceClientFallback.class)
public interface UserServiceClient {

	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsersByLibraryService();

	@GetMapping("/users/{userId}")
	public ResponseEntity<User> getUserByIdByLibraryService(@PathVariable int userId);

	@PostMapping("/users")
	public ResponseEntity<User> addUserByLibraryService(@RequestBody UserDTO userDTO);

	@PutMapping("/users/{userId}")
	public ResponseEntity<User> updateUserByLibraryService(@PathVariable int userId, UserDTO userDTO);

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<?> deleteUserByLibraryService(@PathVariable int userId);
}
