package com.epam.libraryservice.interfaces;

import com.epam.libraryservice.fallbackfactory.BookServiceClientFallBack;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Component
@FeignClient(name = "book-service-h2",fallback = BookServiceClientFallBack.class)
public interface BookServiceClient {

	@GetMapping("/books")
	ResponseEntity<List<Book>> getBooksByLibraryService();

	@GetMapping("/books/{bookId}")
	public ResponseEntity<Book> getBookByIdByLibraryService(@PathVariable int bookId);

	@PostMapping("/books")
	public ResponseEntity<Book> addBookByLibraryService(@RequestBody BookDTO bookToBeAdded);

	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<?> deleteBookByIdByLibraryService(@PathVariable int bookId);
}
