package com.epam.libraryservice.fallbackfactory;

import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;
import com.epam.libraryservice.util.ResponseEntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookServiceClientFallBack implements BookServiceClient {

	private static final Logger LOG = LogManager.getLogger(BookServiceClientFallBack.class);
	private static final String DUMMY = "dummy";


	@Override
	public ResponseEntity<List<Book>> getBooksByLibraryService() {
		List<Book> dummyBooks = new ArrayList<>();
		dummyBooks.add(new Book(-1, DUMMY, DUMMY, (long) 000000, DUMMY));
		return ResponseEntity.ok(dummyBooks);
	}

	@Override
	public ResponseEntity<Book> getBookByIdByLibraryService(int bookId) {
		return ResponseEntityUtils.getServiceUnavailableResponseEntityForBookService();
	}

	@Override
	public ResponseEntity<Book> addBookByLibraryService(BookDTO bookToBeAdded) {
		return ResponseEntityUtils.getServiceUnavailableResponseEntityForBookService();
	}

	@Override
	public ResponseEntity<HttpStatus> deleteBookByIdByLibraryService(int bookId) {
		LOG.info("Service unavailable");
		return ResponseEntity.ok(HttpStatus.SERVICE_UNAVAILABLE);
	}

}
