package com.epam.libraryservice.fallbackfactory;

import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;
import com.epam.libraryservice.util.ResponseEntityUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserServiceClientFallback implements UserServiceClient {
	private static final String DUMMY = "dummy";

	@Override
	public ResponseEntity<List<User>> getAllUsersByLibraryService() {
		List<User> dummyUsers = new ArrayList<>();
		dummyUsers.add(new User(-1, DUMMY, DUMMY));
		return ResponseEntity.ok(dummyUsers);
	}

	@Override
	public ResponseEntity<User> getUserByIdByLibraryService(int userId) {
		return ResponseEntityUtils.getServiceUnavailableResponseEntityForUserService();
	}

	@Override
	public ResponseEntity<User> addUserByLibraryService(UserDTO userDTO) {
		return ResponseEntityUtils.getServiceUnavailableResponseEntityForUserService();
	}

	@Override
	public ResponseEntity<User> updateUserByLibraryService(int userId, UserDTO userDTO) {
		return ResponseEntityUtils.getServiceUnavailableResponseEntityForUserService();
	}

	@Override
	public ResponseEntity<HttpStatus> deleteUserByLibraryService(int userId) {
		return ResponseEntity.ok(HttpStatus.SERVICE_UNAVAILABLE);
	}
}
