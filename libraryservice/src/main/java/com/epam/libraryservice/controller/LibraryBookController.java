package com.epam.libraryservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RefreshScope
@RestController
@RequestMapping("/library")
public class LibraryBookController implements BookServiceClient {

	@Autowired
	BookServiceClient bookServiceClient;

	@ApiOperation(value = "Returns All Books")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieves all books") })
	@Override
	public ResponseEntity<List<Book>> getBooksByLibraryService() {
		return bookServiceClient.getBooksByLibraryService();
	}

	@ApiOperation(value = "Returns a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrives a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@Override
	public ResponseEntity<Book> getBookByIdByLibraryService(int bookId) {
		return bookServiceClient.getBookByIdByLibraryService(bookId);
	}

	@ApiOperation(value = "Adds a Book")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully book is created and added"), })
	@Override
	public ResponseEntity<Book> addBookByLibraryService(BookDTO bookToBeAdded) {
		return bookServiceClient.addBookByLibraryService(bookToBeAdded);
	}

	@ApiOperation(value = "Deletes a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deletes a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@Override
	public ResponseEntity<?> deleteBookByIdByLibraryService(int bookId) {
		return bookServiceClient.deleteBookByIdByLibraryService(bookId);
	}

}
