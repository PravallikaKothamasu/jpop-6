package com.epam.libraryservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RefreshScope
@RestController
@RequestMapping("/library")
public class LibraryUserController implements UserServiceClient {

	@Autowired
	UserServiceClient userServiceClient;

	@ApiOperation(value = "Returns All Users")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieves all users") })
	@Override
	public ResponseEntity<List<User>> getAllUsersByLibraryService() {
		return userServiceClient.getAllUsersByLibraryService();
	}

	@ApiOperation(value = "Returns user with given userId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved user"),
			@ApiResponse(code = 404, message = "User not found with given id") })
	@Override
	public ResponseEntity<User> getUserByIdByLibraryService(int userId) {
		return userServiceClient.getUserByIdByLibraryService(userId);
	}

	@ApiOperation(value = "Adds a user")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created new user") })
	@Override
	public ResponseEntity<User> addUserByLibraryService(UserDTO userDTO) {
		return userServiceClient.addUserByLibraryService(userDTO);
	}

	@ApiOperation(value = "Updates a user having given userId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated user with given userId"),
			@ApiResponse(code = 404, message = "User not found with given user id") })
	@Override
	public ResponseEntity<User> updateUserByLibraryService(int userId, UserDTO userDTO) {
		return userServiceClient.updateUserByLibraryService(userId, userDTO);
	}

	@ApiOperation(value = "Deletes a user with given userId")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deleted user with given userId"),
			@ApiResponse(code = 404, message = "User not found with given user id") })
	@Override
	public ResponseEntity<?> deleteUserByLibraryService(int userId) {
		return userServiceClient.deleteUserByLibraryService(userId);
	}
}
