package com.epam.libraryservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.libraryservice.model.IssueBook;
import com.epam.libraryservice.service.LibraryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/library/users/{userId}/books/{bookId}")
public class LibraryController {

	@Autowired
	LibraryService libraryService;

	@ApiOperation(value = "Issues a book to a user")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully issued the requested book to the user"),
			@ApiResponse(code = 404, message = "If the book is already issued to user or if userId is invalid or if bookId is invalid") })
	@PostMapping
	public ResponseEntity<IssueBook> issueBookWithBookIdToUser(@PathVariable int userId, @PathVariable int bookId) {
		IssueBook issuedBook = libraryService.issueBookToAUser(userId, bookId);
		final java.net.URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{issueId}")
				.buildAndExpand(issuedBook.getIssueId()).toUri();
		return ResponseEntity.created(uri).body(issuedBook);
	}

	@ApiOperation(value = "Deletes a book by given bookId")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Successfully releases a book that user wants to release"),
			@ApiResponse(code = 404, message = "If the book is not issued to user or if userId is invalid or if bookId is invalid") })
	@DeleteMapping
	public ResponseEntity<Object> releaseBookToUser(@PathVariable int userId, @PathVariable int bookId) {
		libraryService.releaseTheBookforUser(userId, bookId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
