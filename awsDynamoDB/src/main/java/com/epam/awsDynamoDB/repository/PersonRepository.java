package com.epam.awsDynamoDB.repository;

import com.epam.awsDynamoDB.entity.Person;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@EnableScan
public interface PersonRepository extends CrudRepository<Person,String> {
}
