package com.epam.awsDynamoDB.controller;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.epam.awsDynamoDB.entity.Person;
import com.epam.awsDynamoDB.repository.PersonRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dynamo-db")
public class AWSDynamoDBController {

    private static final Logger logger = LogManager.getLogger(AWSDynamoDBController.class);

    private DynamoDBMapper dynamoDBMapper;

    private DynamoDB client;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    public AWSDynamoDBController(DynamoDBMapper dynamoDBMapper, DynamoDB client)
    {
        this.dynamoDBMapper = dynamoDBMapper;
        this.client = client;
    }

    @PostMapping("/create-table")
    private String createTable() {
        CreateTableRequest request = dynamoDBMapper.generateCreateTableRequest(Person.class);
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput(1L, 1L);
        request.setProvisionedThroughput(provisionedThroughput);
        Table table = client.createTable(request);
        try {
            table.waitForActive();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "exception occured in creating table";
        }
        return "table created";
    }

    @PostMapping("/insert")
    private ResponseEntity<String> insert(@RequestBody Person person){
        personRepository.save(person);
        return ResponseEntity.ok("inserted");
    }

    @GetMapping("/fetch-all")
    private ResponseEntity<List<Person>> getAll(){
        return ResponseEntity.ok((List<Person>)personRepository.findAll());
    }

    @GetMapping("/fetch/id/{id}")
    private ResponseEntity<Person> getById(@PathVariable String id) {
        return ResponseEntity.ok(personRepository.findById(id).get());
    }

    @PutMapping("/update/id/{id}")
    private ResponseEntity<Person> update(@PathVariable String id) {
        Person person = personRepository.findById(id).get();
        person.setName("kvlss pravallika");
        return ResponseEntity.ok(personRepository.save(person));
    }

    @DeleteMapping("/delete/id/{id}")
    private ResponseEntity<String> delete(@PathVariable String id) {
        personRepository.deleteById(id);
        return ResponseEntity.ok("deleted");
    }
}
